﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using CertificadoPalication.Model;

namespace CertificadoPalication
{
    class Login
    {
        //decalre properties
        public string Username { get; set; }
        public string Userpassword { get; set; }

        //validate string
        private bool EmailValidator(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;
            Regex rgx = new Regex(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$");
            
            if (rgx.IsMatch(email))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool ValidarLogin(string email, string senha)
        {
            //Verificar se o e-mail esta vazio
            if (string.IsNullOrEmpty(email))
            {
                MessageBox.Show("Entre com seu E-mail!");
                return false;

            }
            //Verifica se a senha está vazia
            if (string.IsNullOrEmpty(senha))
            {
                MessageBox.Show("Entre com sua Senha!");
                return false;
            }
            //Verifica se o e-mail é válido
            else if (!EmailValidator(email))
            {

                MessageBox.Show("E-mail invalido!");
                ClearTexts(email, senha);
                return false;

            }

            return true;
        }
        //clear user inputs
        private void ClearTexts(string email, string senha)
        {
            email = String.Empty;
            senha = String.Empty;
        }
        //method to check if elegible to be logged in
        internal User IsLoggedIn(string email, string senha)
        {
            User user = new User();

            if (ValidarLogin(email, senha))
            {
                user = User.GetUserId(email, senha);
                if (user.Id != "")
                {
                    return user;
                }
                else
                {
                    MessageBox.Show("Usuário não encontrado!");
                }
            }

            return new User();
        }
    }
}
