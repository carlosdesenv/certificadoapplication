﻿using CertificadoPalication.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CertificadoPalication
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            
        }
        //Enter code here for your version of username and userpassword
        Login login = new Login();


        private void button1_Click(object sender, EventArgs e)
        {
            //define local variables from the user inputs
            string user = emailtxtbox.Text;
            string pass = senhatxtbox.Text;
            //check if eligible to be logged in
            User userFind = login.IsLoggedIn(user, pass);
            if (userFind.Id != null && userFind.Id != "")
            {
                
                OpenUserForm(userFind);
                fillListCertificados();
            }
            else
            {
                //show default login error message
                MessageBox.Show("Entrada não efetuada!");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            CloseProgram();
        }

        private void OpenUserForm(User user)
        {
            label1.Visible = false;
            label2.Visible = false;
            emailtxtbox.Visible = false;
            senhatxtbox.Visible = false;
            emailtxtbox.Text = "";
            senhatxtbox.Text = "";
            button1.Visible = false;

            if(user.Tipo != "Aprovador")
            {
                label5.Visible = true;
                label6.Visible = true;
                label7.Visible = true;
                label8.Visible = true;
                label9.Visible = true;
                label3.Visible = true;
                label4.Text = user.Nome;
                label4.Visible = true;
                button2.Visible = true;
                nome_curso.Visible = true;
                instituicao.Visible = true;
                descricao.Visible = true;
                datafim.Visible = true;
                datainicio.Visible = true;
                button3.Visible = true;
                btnDelete.Visible = true;
                
            }
            else
            {
                brnAprovar.Visible = true;
                this.brnReprovar.Visible = true;
            }

            button4.Visible = true;
            dataGridView1.Visible = true;
        }

        private void CloseProgram()
        {
            label1.Visible = true;
            label2.Visible = true;
            emailtxtbox.Visible = true;
            senhatxtbox.Visible = true;
            button1.Visible = true;

            label5.Visible = false;
            label6.Visible = false;
            label7.Visible = false;
            label8.Visible = false;
            label9.Visible = false;
            label3.Visible = false;
            label4.Text = "";
            label4.Visible = false;
            button2.Visible = false;
            nome_curso.Visible = false;
            instituicao.Visible = false;
            descricao.Visible = false;
            datainicio.Visible = false;
            datafim.Visible = false;
            button3.Visible = false;
            dataGridView1.Visible = false;
            brnAprovar.Visible = false;
            this.brnReprovar.Visible = false;
            btnDelete.Visible = false;

        }

        private void button3_Click(object sender, EventArgs e)          
        {
            if (VerificarPreenchimento())
            {
                string pathFrom = @"C:\certificados";

                OpenFileDialog openFileDialog1 = new OpenFileDialog();
                if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    System.IO.Directory.CreateDirectory(pathFrom);
                    File.Copy(openFileDialog1.FileName, pathFrom + "\\" + openFileDialog1.SafeFileName, true);
                    
                    SaveCertificado("C:\\\\certificados\\\\" + openFileDialog1.SafeFileName);
                    fillListCertificados();
                }
                else
                {
                    MessageBox.Show("Necessário selecionar um arquivo!");
                }

            }

        }

        private void DataGridView_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {   if(e.RowIndex != -1)
            {
                DataGridViewRow row = dataGridView1.Rows[e.RowIndex];
            }
           
           
        }

        private void SaveCertificado(string caminhoArquivo)
        {
            Certificado certificado = new Certificado();

            certificado.NomeCurso = nome_curso.Text;
            certificado.Instituicao = instituicao.Text;
            certificado.DataInicio = datainicio.Value;
            certificado.DataFim = datafim.Value;
            certificado.Descricao = descricao.Text;
            certificado.CaminhoArquivo = caminhoArquivo;

            Certificado.Add(certificado);

            nome_curso.Text = "";
            instituicao.Text = "";
            datainicio.Value = DateTime.Today;
            datafim.Value = DateTime.Today;
            descricao.Text = "";
        }

        private bool VerificarPreenchimento()
        {

            if (String.IsNullOrEmpty(nome_curso.Text))
            {
                MessageBox.Show("Preencha o nome do curso");
                return false;
            }

            if (String.IsNullOrEmpty(instituicao.Text))
            {
                MessageBox.Show("Preencha o nome da instituição");
                return false;
            }

            if (String.IsNullOrEmpty(descricao.Text))
            {
                MessageBox.Show("Preencha a descrição");
                return false;
            }
            if (datainicio.Value == null)
            {
                MessageBox.Show("Preencha a data de início");
                return false;
            }
            if (datafim.Value == null)
            {
                MessageBox.Show("Preencha a data de termino");
                return false;
            }

            return true;
        }

        private void fillListCertificados()
        {
            List<Certificado> certificados = Certificado.GetAll();
            dataGridView1.DataSource = certificados;
        }

        private void ViewFile_Click(object sender, EventArgs e)
        {
            int rowindex = this.dataGridView1.CurrentCell.RowIndex;
            if(this.dataGridView1.Rows[rowindex].Cells[6].Value != null){
                System.Diagnostics.Process.Start(this.dataGridView1.Rows[rowindex].Cells[6].Value.ToString());
            }
            
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int rowindex = this.dataGridView1.CurrentCell.RowIndex;

            Certificado.Delete(this.dataGridView1.Rows[rowindex].Cells[0].Value.ToString());
            List<Certificado> certificados = Certificado.GetAll();
            dataGridView1.DataSource = certificados;
        }
    }

        private void btnAprovar_Click(object sender, EventArgs e)
        {

        }

        private void btnReprovar_Click(object sender, EventArgs e)
        {

        }
    }
}
