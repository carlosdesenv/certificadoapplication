﻿using CertificadoPalication.Service;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CertificadoPalication.Model
{
    class Certificado
    {
        public string Id { get; set; }
        public string NomeCurso { get; set; }
        public string Instituicao { get; set; }
        public DateTime DataInicio { get; set; }
        public DateTime DataFim { get; set; }
        public string Descricao { get; set; }
        public string CaminhoArquivo { get; set; }

        public static void Add(Certificado certificado)
        {
            var connection = Connection.GetConnection();
            var command = connection.CreateCommand();

            try
            {
                connection.Open();

                string sql = "INSERT INTO certificados (nome_curso,instituicao,data_inicio,data_termino,descricao,caminho_arquivo) " +
                                            "VALUES ('" + certificado.NomeCurso + "','" + certificado.Instituicao + "'" +
                                            ",'" + certificado.DataInicio.ToString("yyyy-MM-dd H:mm:ss") + "','" 
                                            + certificado.DataFim.ToString("yyyy-MM-dd H:mm:ss") + "','" + certificado.Descricao + "'" +
                                            ",'" + certificado.CaminhoArquivo + "')";

                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();

            }
            finally{
                connection.Close();
            }
      
           
        }
        public static List<Certificado> GetAll()
        {
            var connection = Connection.GetConnection();
            var command = connection.CreateCommand();

            try
            {
                List<Certificado> listaCertidifados = new List<Certificado>();
                
                connection.Open();

                string sql = "SELECT * FROM certificados";

                MySqlCommand cmd = new MySqlCommand(sql, connection);
                MySqlDataReader retorno = cmd.ExecuteReader();

                while (retorno.Read())
                {
                    Certificado certificado = new Certificado();

                    certificado.Id = retorno[0].ToString();
                    certificado.NomeCurso = retorno[1].ToString();
                    certificado. Instituicao = retorno[2].ToString();
                    certificado.DataInicio = DateTime.Parse(retorno[3].ToString());
                    certificado.DataFim = DateTime.Parse(retorno[4].ToString());
                    certificado.Descricao = retorno[5].ToString();
                    certificado.CaminhoArquivo = retorno[6].ToString();

                    listaCertidifados.Add(certificado);
                }

                retorno.Close();
                return listaCertidifados;

            }
            finally
            {
                connection.Close();
            }

        }
        public static void Delete(string id)
        {
            var connection = Connection.GetConnection();
            var command = connection.CreateCommand();

            try
            {
                connection.Open();

                string sql = "DELETE FROM certificados where id = " + id;

                MySqlCommand cmd = new MySqlCommand(sql, connection);
                cmd.ExecuteNonQuery();

            }
            finally
            {
                connection.Close();
            }


        }
    }

    
}
